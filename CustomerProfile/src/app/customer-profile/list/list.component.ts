import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { CustomerProfileComponent } from '../customer-profile.component';
import { DeleteCustomerComponent } from '../delete-customer/delete-customer.component';
import { CustomerProfileService } from '../services/customer-profile.service';
import {Sort} from '@angular/material/sort';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  dataSource: any ; 
  displayedColumns: string[] = [
    'firstName',
    'lastName',
    'email',
    'mobile',
    'dateOfBirth',
    'city',
    'actions'
  ];
  itemsInPageList:Array<number> = [5,10,50,100];
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  tRowNoRecord:boolean =false;
  constructor( public dialog: MatDialog,
    private customerService: CustomerProfileService,
    private datePipe : DatePipe) {
  }

  ngOnInit(): void {
    this.loadCustomerProfileList();
  }

  sortData(sort: Sort) {
    const data = this.dataSource.data.slice();
    if (!sort.active || sort.direction === '') {
      this.dataSource.data = data;
      return;
    }
    this.dataSource.data = data.sort((a?:any, b?:any) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'firstName': return compare(a.FirstName, b.FirstName, isAsc);
        case 'lastName': return compare(a.LastName, b.LastName, isAsc);
        case 'email': return compare(a.Email, b.Email, isAsc);
        case 'mobile': return compare(a.Mobile, b.Mobile, isAsc);
        case 'dateOfBirth': return compare(a.DOB, b.DOB, isAsc);
        case 'city': return compare(a.City, b.City, isAsc);
        default: return 0;
      }
    });
  }

  loadCustomerProfileList(){
    this.customerService.getCustomerProfileList().subscribe(res =>{
      if(res){
        this.tRowNoRecord = res.length==0;
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }else{
        this.dataSource = new MatTableDataSource([]);
      }
    },err=>{
       
    });    	
  }

  openAddModal(data?:any) {
    const diaref = this.dialog.open(CustomerProfileComponent, {
      width: '500px',
      disableClose: true,
      data:data
    });
    diaref.afterClosed().subscribe((val:any)=> {
      if(val==1){
        this.loadCustomerProfileList();
      }
    });
  }

  openDeleteModal(data?:any) {
    const diaref = this.dialog.open(DeleteCustomerComponent, {
      width: '500px',
      disableClose: true,
      data: data 
    });
    diaref.afterClosed().subscribe((val:any)=> {
      if(val==1){
        this.loadCustomerProfileList();
      }
    });
  }
}

function compare(a: number | string , b: number | string , isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}