import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import Swal from 'sweetalert2';
import { CustomerProfileService } from '../services/customer-profile.service';

@Component({
  selector: 'app-delete-customer',
  templateUrl: './delete-customer.component.html',
  styleUrls: ['./delete-customer.component.scss']
})
export class DeleteCustomerComponent implements OnInit {

  constructor(
    private customerService: CustomerProfileService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<DeleteCustomerComponent>
  ) { }

  ngOnInit(): void {
  }

  deleteCustomer(){
    this.customerService.deleteCustomerProfile(this.data.Id).subscribe(res =>{
      if(res[0].Status==1){
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Deleted Successfully.',
          showConfirmButton: false,
          timer: 3000
        })
        this.dialogRef.close(1);
      }
      else{
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Deleted Failed.',
          showConfirmButton: false,
          timer: 3000
        })
      }
    },()=>{
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Deleted Failed.',
        showConfirmButton: false,
        timer: 3000
      })
    });
  }

}
