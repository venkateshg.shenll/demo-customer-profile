import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CustomerProfileService } from './services/customer-profile.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import Swal from 'sweetalert2';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-customer-profile',
  templateUrl: './customer-profile.component.html',
  styleUrls: ['./customer-profile.component.scss']
})
export class CustomerProfileComponent implements OnInit {
  customerform = new FormGroup({
    firstName: new FormControl('', [Validators.required]),
    lastName: new FormControl(''),
    email: new FormControl('', [Validators.required, Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
    mobile:  new FormControl('', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]),
    city : new FormControl('', Validators.required),
    dateOfBirth : new FormControl('', Validators.required),
    address1 : new FormControl('' , Validators.required),
    address2 : new FormControl('')
  })

  constructor(
    private customerService: CustomerProfileService,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<CustomerProfileComponent>,
    private datePipe : DatePipe
  ) { 
  }

  ngOnInit(): void {
    if(this.data && this.data.Id){
      this.customerform.patchValue({
        firstName :  this.data.FirstName,
        lastName :  this.data.LastName,
        mobile :  this.data.Mobile,
        email:  this.data.Email,
        dateOfBirth :  this.data.DOB,
        address1 :  this.data.Address1,
        address2 :  this.data.Address2,
        city :  this.data.City
      })
    }
  }

  get f () {
    return this.customerform.controls;
  }

  submitCustomer(){
    let form1 = this.customerform.value;
    if(this.customerform.valid){
      let sendData = {
        Id: this.data?.Id,
        FirstName :  form1.firstName,
        LastName :  form1.lastName,
        Mobile :  form1.mobile,
        Email:  form1.email,
        DOB : this.datePipe.transform(form1.dateOfBirth, 'MM-dd-yyyy'), //form1.dateOfBirth,
        Address1 :  form1.address1,
        Address2 :  form1.address2,
        City :  form1.city
      }
      if(this.data && this.data.Id){
        this.customerService.updateCustomerProfile(sendData).subscribe(res =>{
          if(res[0].Status==1){
            Swal.fire({
              position: 'top-end',
              icon: 'success',
              title: 'Updated Successfully.',
              showConfirmButton: false,
              timer: 3000
            })
            this.dialogRef.close(1);
          }
          else{
            Swal.fire({
              position: 'top-end',
              icon: 'success',
              title: 'Update Failed.',
              showConfirmButton: false,
              timer: 3000
            })
          }
        },()=>{
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Update Failed.',
            showConfirmButton: false,
            timer: 3000
          })
        });
      }
      else{
        this.customerService.addCustomerProfile(sendData).subscribe(res =>{
          if(res[0].Status==1){
            Swal.fire({
              position: 'top-end',
              icon: 'success',
              title: 'Saved Successfully.',
              showConfirmButton: false,
              timer: 3000
            })
            this.dialogRef.close(1);
          }
          else{
            Swal.fire({
              position: 'top-end',
              icon: 'success',
              title: 'Save Failed.',
              showConfirmButton: false,
              timer: 3000
            })
          }
        },()=>{
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Save Failed.',
            showConfirmButton: false,
            timer: 3000
          })
        });
      }
    }
  }
}
