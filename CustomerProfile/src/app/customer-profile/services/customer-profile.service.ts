import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class CustomerProfileService {
  base_url: string = "https://localhost:44336/api/";

  constructor(private http:HttpClient) { }

  getCustomerProfileList(_id?:number){
    return this.http.get(this.base_url + 'getcustomerprofile').pipe(map((res:any) => res));
  }

  addCustomerProfile(data: any){
    return this.http.post(`${this.base_url}`+'addcustomerprofile',data).pipe(map((res:any)=>res));
  }

  updateCustomerProfile(data: any){
    return this.http.put(`${this.base_url}`+'updatecustomerprofile',data).pipe(map((res:any)=>res));
  }

  deleteCustomerProfile(id:number){
    return this.http.delete(`${this.base_url}`+'deletecustomerprofile/'+id).pipe(map((res:any) => res));
  }
}
