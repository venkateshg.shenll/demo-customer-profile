-- select * from Customer_Profile_Tbl

CREATE TABLE Customer_Profile_Tbl
(
	Id int identity(1,1) NOT NULL,
	FirstName VARCHAR(50) NULL,
	LastName VARCHAR(50) NULL,
	Mobile VARCHAR(10) NULL,
	Email VARCHAR(50) NULL,
	DOB DATETIME NULL,
	Address1 VARCHAR(200) NULL,
	Address2 VARCHAR(200) NULL,
	City VARCHAR(100) NULL 
)
 