﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Runtime.InteropServices;
using CRUDApi.Models;
namespace CRUDApi.Models
{
    public class AngularContext: DbContext
    {
        public AngularContext([Optional, DefaultParameterValue("DbContext")] string DB)
                 : base(DB)
        {
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Customer_Profile>().HasKey(x => new { x.Id });
        }
        public DbSet<Customer_Profile> CustomerProfileData { get; set; }
    }
}