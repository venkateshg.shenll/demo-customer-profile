﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CRUDApi.Models
{
    public class CustomerProfile
    {
    }
    //public class CustomerProfileList
    //{
    //    public List<Customer_Profile> Data { get; set; }
    //}
    [Table("Customer_Profile_Tbl")]
    public class Customer_Profile
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public DateTime DOB { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
    }

    public class ResponseMessage
    {
        public int Status { get; set; }
        public string Message { get; set; }
    }

    //public class CustomerData
    //{
    //    public string FirstName { get; set; }
    //    public string LastName { get; set; }
    //    public string Mobile { get; set; }
    //    public string Email { get; set; }
    //    public DateTime DOB { get; set; }
    //    public string Address1 { get; set; }
    //    public string Address2 { get; set; }
    //    public string City { get; set; }
    //    public string ProfileImg { get; set; }
    //}
}