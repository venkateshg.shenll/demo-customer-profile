﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CRUDApi.Models;
using System.Threading.Tasks;


namespace CRUDApi.Controllers
{
    public class CustomerProfileController : ApiController
    {
        [Route("api/getcustomerprofile/{id?}")]
        public async Task<List<Customer_Profile>> GetCustomerProfile(int Id=0)
        {
            List<Customer_Profile> CustomerProfileList = new List<Customer_Profile>();
            try
            {
                using (AngularContext db = new AngularContext())
                {
                    if(Id == 0)
                        CustomerProfileList = db.CustomerProfileData.Where(a => a.Id > 0).OrderBy(a => a.FirstName).ToList();
                    else
                        CustomerProfileList = db.CustomerProfileData.Where(a => a.Id == Id).OrderBy(a => a.FirstName).ToList();
                }
            }
            catch (Exception ex)
            {

            }
            return CustomerProfileList;
        }

        [HttpPost]
        [Route("api/addcustomerprofile")]
        public async Task<List<ResponseMessage>> AddCustomerProfile(Customer_Profile ProfileData)
        {
            List<ResponseMessage> results = new List<ResponseMessage>();
            ResponseMessage objmsg = new ResponseMessage();
            try
            {
                if (ProfileData.FirstName != "" && ProfileData.LastName != "" && ProfileData.Mobile != ""
                    && ProfileData.Email != "" && ProfileData.Mobile != "" && Convert.ToString(ProfileData.DOB) != ""
                    && ProfileData.Address1 != "" && ProfileData.City != "")
                {
                    using (AngularContext dbContext = new AngularContext())
                    {
                        Customer_Profile obj = new Customer_Profile();
                        obj.FirstName = ProfileData.FirstName;
                        obj.LastName = ProfileData.LastName;
                        obj.Mobile = ProfileData.Mobile;
                        obj.Email = ProfileData.Email;
                        obj.DOB = Convert.ToDateTime(ProfileData.DOB);
                        obj.Address1 = ProfileData.Address1;
                        obj.Address2 = ProfileData.Address2;
                        obj.City = ProfileData.City;
                        //obj.ProfileImg = ProfileData.ProfileImg;

                        dbContext.CustomerProfileData.Add(obj);
                        dbContext.SaveChanges();

                        objmsg.Status = 1;
                        objmsg.Message = "Record Save Successfully.";
                        results.Add(objmsg);
                    }
                }
                else
                {
                    objmsg.Status = 0;
                    objmsg.Message = "Mandatory Field Value Missing.";
                    results.Add(objmsg);
                }
            }
            catch (Exception ex)
            {
                objmsg.Status = 0;
                objmsg.Message = "Record save failure.";
                results.Add(objmsg);
            }
            return results;
        }

        [HttpPut]
        [Route("api/updatecustomerprofile")]
        public async Task<List<ResponseMessage>> UpdateCustomerProfile(Customer_Profile ProfileData)
        {
            List<ResponseMessage> results = new List<ResponseMessage>();
            ResponseMessage objmsg = new ResponseMessage();
            try
            {
                if(ProfileData.FirstName!=""&& ProfileData.LastName != "" && ProfileData.Mobile!=""
                    && ProfileData.Email != "" && ProfileData.Mobile != "" && Convert.ToString(ProfileData.DOB) != ""
                    && ProfileData.Address1!="" && ProfileData.City!="")
                {
                    using (AngularContext dbContext = new AngularContext())
                    {
                        List<Customer_Profile> Profile = dbContext.CustomerProfileData.Where(a => a.Id == ProfileData.Id).ToList();

                        foreach (var objProfile in Profile)
                        {
                            objProfile.FirstName = ProfileData.FirstName;
                            objProfile.LastName = ProfileData.LastName;
                            objProfile.Mobile = ProfileData.Mobile;
                            objProfile.Email = ProfileData.Email;
                            objProfile.DOB = Convert.ToDateTime(ProfileData.DOB);
                            objProfile.Address1 = ProfileData.Address1;
                            objProfile.Address2 = ProfileData.Address2;
                            objProfile.City = ProfileData.City;
                        }
                        dbContext.SaveChanges();
                    }
                    objmsg.Status = 1;
                    objmsg.Message = "Record Updated Successfully.";
                    results.Add(objmsg);
                }
                else
                {
                    objmsg.Status = 0;
                    objmsg.Message = "Mandatory Field Value Missing.";
                    results.Add(objmsg);
                }
            }
            catch (Exception ex)
            {
                objmsg.Status = 0;
                objmsg.Message = "Record Updated Failure.";
                results.Add(objmsg);
            }
            return results;
        }

        [HttpDelete]
        [Route("api/deletecustomerprofile/{id?}")]
        public async Task<List<ResponseMessage>> DeleteCustomerProfile(int id)
        {
            List<ResponseMessage> results = new List<ResponseMessage>();
            ResponseMessage objmsg = new ResponseMessage();
            try
            {
                using (AngularContext dbContext = new AngularContext())
                {
                    var profile = dbContext.CustomerProfileData.Where(a => a.Id == id).FirstOrDefault();
                    if (profile != null)
                    {
                        dbContext.CustomerProfileData.Remove(profile);
                        dbContext.SaveChanges();
                        objmsg.Status = 1;
                        objmsg.Message = "Record Deleted Successfully.";
                        results.Add(objmsg);
                    }
                    else
                    {
                        objmsg.Status = 0;
                        objmsg.Message = "Record Deleted Failure.";
                        results.Add(objmsg);
                    }
                }
            }
            catch (Exception ex)
            {
                objmsg.Status = 0;
                objmsg.Message = "Record Deleted Failure.";
                results.Add(objmsg);
            }
            return results;
        }
    }
}

